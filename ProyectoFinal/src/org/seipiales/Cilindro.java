/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.seipiales;

import com.sun.opengl.util.GLUT;
import javax.media.opengl.GL;

/**
 * Cilindro.java 
 * Autora:Sandra Ipiales.
 * Este programa contiene los metodos 
 * relacionados con cilindro
 * 2020-09-28
 * 
 */
public class Cilindro {
    //Variables Cilindro 
    double r,h; //Radio, Altura
    int sl,st; //Rebanadas
    //Variables glut,gl
    GL gl;
    GLUT glut;
    //Variables color
    float c1,c2,c3;
    //Variables de transformacion
    float x,y,z, ang;
    
    
    public Cilindro(GL gl, GLUT glut, double r, double h, int sl, int st, float c1, float c2, float c3){
        this.gl = gl;
        this.glut= glut;
        this.r=r;
        this.h=h;
        this.sl= sl;
        this.st=st;
        this.c1=c1;
        this.c2=c2;
        this.c3=c3;
    }
    public void DibujarCilindro(float ang, float x, float y, float z){
        gl.glPushMatrix();
        gl.glTranslatef(x, y, z);
        gl.glRotatef(ang, 1, 0, 0);
        gl.glColor3f(c1,c2,c3);
        glut.glutSolidCylinder(r, h, sl, st); 
        gl.glPopMatrix();
    }
    public void DibujarCilindro(float rotx,float roty, float rotz, float x, float y, float z){
        gl.glPushMatrix();
        gl.glTranslatef(x, y, z);
        gl.glRotatef(rotx, 1, 0, 0);
        gl.glRotatef(roty, 0, 1, 0);        
        gl.glRotatef(rotz, 0, 0, 1);
        gl.glColor3f(c1,c2,c3);
        glut.glutSolidCylinder(r, h, sl, st);  
        gl.glColor3f(0,0,0);
        glut.glutWireCylinder(r, h, sl,st);
        gl.glPopMatrix();
    }
    public void DibujarCilindroB(float rotx,float roty, float rotz, float x, float y, float z){
        gl.glPushMatrix();
        gl.glTranslatef(x, y, z);
        gl.glRotatef(rotx, 1, 0, 0);
        gl.glRotatef(roty, 0, 1, 0);        
        gl.glRotatef(rotz, 0, 0, 1);
        gl.glColor3f(c1,c2,c3);
        glut.glutWireCylinder(r, h, sl, st);  
        gl.glPopMatrix();
    }
}
