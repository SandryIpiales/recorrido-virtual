/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.seipiales;

import com.sun.opengl.util.GLUT;
import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;

/**
 *
 * @author Sandra Ipiales
 * Esta Clase proporciona la figura GLUT Sphere 
 * 2020-09-29
 */
public class Sphere {
    GLU glu;
    GLUT glut;
    GL gl;
    float x, y, z, c1,c2,c3;
    double size;
    int slices, stacks;
    public Sphere(GL gl,GLUT glut, double size, int slices, int stacks, float c1, float c2, float c3){
        this.gl=gl;
        this.glut=glut;
        this.size=size;
        this.slices=slices;
        this.stacks= stacks;
        this.c1=c1;
        this.c2=c2;
        this.c3=c3;
    }
    
    public void DibujarSphere(float x, float y , float z){
        gl.glPushMatrix();
        gl.glTranslatef(x, y, z);
        gl.glColor3f(c1,c2,c3);
        glut.glutSolidSphere(size,slices, stacks);
        gl.glPopMatrix();
    }
     public void DibujarSphereB(float escz,float x, float y , float z, float c4, float c5, float c6){
        gl.glPushMatrix();
        gl.glScalef(1,1, escz);
        gl.glTranslatef(x, y, z);
        gl.glColor3f(c1,c2,c3);
        glut.glutSolidSphere(size,slices, stacks);
        gl.glColor3f(c4,c5,c6);
        glut.glutWireSphere(size, slices, stacks);
        gl.glPopMatrix();
    }
}
