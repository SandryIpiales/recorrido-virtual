package org.seipiales;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import com.sun.opengl.util.GLUT;
import javax.media.opengl.GL;

/**
 *
 * Cono.java 
 * Autora:Sandra Ipiales.
 * Este programa contiene los metodos para 
 * dibujar conos
 * 2020-09-28
 */
public class Cono {
    //Variables Cono 
    double r,h; //Radio, Altura
    int sl,st; //Rebanadas, 
    //Variables gl,glu
    GL gl;
    GLUT glut;
    //Variables de color
    float c1,c2,c3;
    //Variables de transformacion
    float x,y,z, ang;
    
    public Cono(GL gl, GLUT glut, double r,double h, int sl, int st, float c1, float c2, float c3){
       this.gl = gl;
        this.glut= glut;
        this.r=r;
        this.h=h;
        this.sl= sl;
        this.st=st;
        this.c1=c1;
        this.c2=c2;
        this.c3=c3; 
    }
    public void DibujarCono(float ang, float x,float y,float z){
            gl.glPushMatrix();
            gl.glTranslatef(x, y, z);
            gl.glColor3f(c1,c2,c3);
            gl.glRotatef(ang, 1, 0, 0);
            glut.glutSolidCone(r, h, sl, st); 
            gl.glColor3f(0f, 0f, 0f);
            glut.glutWireCone(r, h, sl, st);
            gl.glPopMatrix(); 
        }
}
