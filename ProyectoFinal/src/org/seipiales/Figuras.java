/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.seipiales;

import com.sun.opengl.util.GLUT;
import static java.lang.Math.sin;
import static java.lang.StrictMath.cos;
import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;

/**
 *
 * Figuras.java 
 * Autora:Sandra Ipiales.
 * Esta clase contiene los metodos de figuras GL 
 * 2020-09-28
 */
public class Figuras {
  
    
    public static float w, h;// Ancho y Largo
    public static float x,y,z; //Coordenadas x,y,z
    public static float c1,c2, c3; //RGB
    public static float a,b,c,d;
    
        public Figuras( ){
        }

        public void CuadradoPiso(GL gl,float x, float y, float z,float w, float h,float c1, float c2, float c3){
            gl.glPushMatrix();
            gl.glTranslatef(x, y, z);
            float w1=w/2;
            float h1= h/2;
            // Variables (a,b) Ancho y (c,d) Largo
            gl.glBegin(GL.GL_QUADS);
            gl.glColor3f(c1, c2, c3); //Color
            gl.glVertex3f(w, h, 0);   //Esquina Superior Izquierda
            gl.glVertex3f(-w, h, 0);   //Esquina Superior Derecha
            gl.glColor3f(0.53f, 0.3715f, 0.1166f);
            gl.glVertex3f(-w, -h, 0);   // Esquina Inferior Derecha
            gl.glVertex3f(w, -h, 0);   //Esquina Inferior Izquierda
            gl.glEnd();
            gl.glPopMatrix();
        }
         public void Cuadrado(GL gl,float x, float y, float z,float w, float h, float c1, float c2, float c3){
            gl.glPushMatrix();
            gl.glTranslatef(x, y, z);
            float w1=w/2;
            float h1= h/2;
            // Variables (a,b) Ancho y (c,d) Largo
            gl.glBegin(GL.GL_QUADS);
            gl.glColor3f(c1, c2, c3); //Color
            gl.glVertex3f(w, h, 0);   //Esquina Superior Izquierda
            gl.glVertex3f(-w, h, 0);   //Esquina Superior Derecha
            gl.glVertex3f(-w, -h, 0);   // Esquina Inferior Derecha
            gl.glVertex3f(w, -h, 0);   //Esquina Inferior Izquierda
            gl.glEnd();
            gl.glPopMatrix();
        }
        public void CuadradoBorde(GL gl,float x, float y, float z,float w, float h, float c1, float c2, float c3){
            gl.glPushMatrix();
            gl.glTranslatef(x, y, z);
            float w1=w/2;
            float h1= h/2;
            // Variables (a,b) Ancho y (c,d) Largo
            gl.glBegin(GL.GL_LINES);
            gl.glColor3f(c1, c2, c3); //Color
            gl.glVertex3f(w, h, 0);   //Esquina Superior Izquierda
            gl.glVertex3f(-w, h, 0);   //Esquina Superior Derecha
            gl.glVertex3f(-w, -h, 0);   // Esquina Inferior Derecha
            gl.glVertex3f(w, -h, 0);   //Esquina Inferior Izquierda
            gl.glEnd();
            gl.glPopMatrix();
        }
        
        public void Poligono(GL gl,float x, float y, float z,float w, float h, float c1, float c2, float c3){
            
            gl.glTranslatef(x, y, z);
            float w1=w/2;
            float h1= h/2;
            // Variables (a,b) Ancho y (c,d) Largo
            gl.glBegin(GL.GL_POLYGON);
            gl.glColor3f(c1, c2, c3); //Color
            gl.glVertex3f(w, h, 0);   //Esquina Superior Izquierda
            gl.glVertex3f(-w, h, 0);   //Esquina Superior Derecha
            gl.glVertex3f(w/2, h, z);
            gl.glVertex3f(-w, -h, 0);
            gl.glVertex3f(w/2, -h, z);   // Esquina Inferior Derecha
            gl.glVertex3f(w, -h, 0);   //Esquina Inferior Izquierda
            
            gl.glEnd();
        }
        
        //Circulo 
        public void Circulo(GL gl,float a, float b, float c, float c1, float c2, float c3, float x,float y, float z){
           gl.glPushMatrix();
            gl.glBegin(GL.GL_POLYGON);
            gl.glColor3f(c1, c2, c3);
            for (int i = 0; i < 1000; i++) {
                x=(float)(c*Math.cos(i));
                y=(float)(c*Math.sin(i));
                gl.glVertex3f(x+a, y+b, z);
            }
            gl.glEnd();
            gl.glPopMatrix();
        }
         public void DibujarTriangulo(GL gl,float a, float b, float c,float d, float e, float c1, float c2, float c3, float x, float y, float z){
            gl.glPushMatrix();
            gl.glTranslatef(x, y, z);
            gl.glBegin(GL.GL_TRIANGLES);
            
                gl.glColor3f(c1, c2, c3); //Color
                gl.glVertex3f(a, d, 0);   //Vertice Superior
                gl.glVertex3f(b, e, 0);   //Vertice Inferior Izuierdo
                gl.glVertex3f(c, e, 0);   //Vertice Inferior Derecho
            gl.glEnd();
            gl.glPopMatrix();
        }
         
        public void dibujarCubo(GL gl, float x, float y, float z, float w, float h, float d, float c1, float c2, float c3){
       
            gl.glPushMatrix();
         gl.glTranslatef(x,y,z);
            float w1=w/2;
            float h1=h/2;
            float d1=d/2;

            //BASE
            gl.glColor3f(c1,c2,c3); 
            gl.glBegin(gl.GL_QUADS);

            
            gl.glVertex3f(0-w1, 0-h1, 0+d1);
            gl.glVertex3f(0+w1, 0-h1, 0+d1);
            gl.glVertex3f(0+w1, 0-h1, 0-d1);
            gl.glVertex3f(0-w1, 0-h1, 0-d1);
            gl.glEnd();

            //SUPERIOR
            gl.glBegin(gl.GL_QUADS);
            gl.glVertex3f(0-w1, 0+h1, 0+d1);
            gl.glVertex3f(0+w1, 0+h1, 0+d1);
            gl.glVertex3f(0+w1, 0+h1, 0-d1);
            gl.glVertex3f(0-w1, 0+h1, 0-d1);
            gl.glEnd();

            //FRONTAL
            gl.glBegin(gl.GL_QUADS);
            gl.glColor3f(c1,c2,c3);
            gl.glVertex3f(0-w1, 0-h1, 0+d1);
            gl.glVertex3f(0+w1, 0-h1, 0+d1);
            gl.glVertex3f(0+w1, 0+h1, 0+d1);
            gl.glVertex3f(0-w1, 0+h1, 0+d1);
            gl.glEnd();

            //POSTERIOR
            gl.glBegin(gl.GL_QUADS);
            gl.glVertex3f(0-w1, 0-h1, 0-d1);
            gl.glVertex3f(0+w1, 0-h1, 0-d1);
            gl.glVertex3f(0+w1, 0+h1, 0-d1);
            gl.glVertex3f(0-w1, 0+h1, 0-d1);
            gl.glEnd();

            //LATERAL IZQUIERDA
            gl.glBegin(gl.GL_QUADS);
            gl.glVertex3f(0-w1, 0-h1, 0+d1);
            gl.glVertex3f(0-w1, 0-h1, 0-d1);
            gl.glVertex3f(0-w1, 0+h1, 0-d1);
            gl.glVertex3f(0-w1, 0+h1, 0+d1);
            gl.glEnd();

            //LATERAL DERECHA
            gl.glBegin(gl.GL_QUADS);
            gl.glVertex3f(0+w1, 0-h1, 0+d1);
            gl.glVertex3f(0+w1, 0-h1, 0-d1);
            gl.glVertex3f(0+w1, 0+h1, 0-d1);
            gl.glVertex3f(0+w1, 0+h1, 0+d1);
            gl.glEnd();
            gl.glPopMatrix();
        }
    }
