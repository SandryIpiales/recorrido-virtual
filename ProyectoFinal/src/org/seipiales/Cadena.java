/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.seipiales;

import com.sun.opengl.util.GLUT;
import javax.media.opengl.GL;

/**
 *
 * Cadena.java 
 * Autora:Sandra Ipiales.
 * Esta clase contiene la implementacion de cadenas
 * 
 * 2020-09-29
 */
public class Cadena {
    GL gl;
    GLUT glut;
    Torus cad;
    float x,y,z;
    
    public Cadena(GL gl, GLUT glut){
        this.gl= gl;
        this.glut= glut;
       
        cad = new Torus(gl,glut, 0.1,0.3f, 10,10);
    }
    
    public void DibujarCad( float x, float y, float z, float c1, float c2, float c3){
        
        gl.glPushMatrix();
        gl.glTranslatef(x, y, z);
        gl.glRotatef(80, 0,1,1);
        float posx=0, posy=0;
        for (int i = 0; i < 8; i++) {
        cad.DibujarTorus(posx, posy, -0.2f, c1,c2,c3,40);
        posx-=0.5f;
        posy-=0.2f;
        }

        gl.glPopMatrix();
    }
    
}
