/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.seipiales;

import com.sun.opengl.util.GLUT;
import javax.media.opengl.GL;

/**
 *
 * Cubo.java 
 * Autora:Sandra Ipiales.
 * Esta clase contiene los metodos de figura GLUT CUBO 
 * 2020-09-28
 */
public class Cubo {
    //Variables gl, glut
    GL gl;
    GLUT glut;
    //Variable para Cubo
    float size;
    //Variables de translacion
    float x, y, z;
    //Variables de color
    float c1,c2,c3,c4,c5,c6;
    
    public Cubo(GL gl,GLUT glut, float size, float c1, float c2, float c3){
        this.gl=gl;
        this.glut=glut;
        this.size=size;
        this.c1=c1;
        this.c2=c2;
        this.c3=c3;
        
    }
    public void DibujarCuboBorde(float x, float y , float z, float c4,float c5, float c6){
        gl.glPushMatrix();
        gl.glTranslatef(x, y, z);
          gl.glColor3f(c1,c2,c3);
          glut.glutSolidCube(size);
          gl.glColor3f(c4,c5,c6);
        glut.glutWireCube(size);
      
        gl.glPopMatrix();
    }
    public void DibujarCubo(float x, float y , float z){
        gl.glPushMatrix();
        gl.glTranslatef(x, y, z);
          gl.glColor3f(c1,c2,c3);
          glut.glutSolidCube(size);
        gl.glPopMatrix();
    }
    public void DibujarColumna(int a,float x, float y , float z, float c4,float c5, float c6){
        gl.glPushMatrix();
        gl.glTranslatef(x, y, z);
        for (int i = 0; i < a; i++) {
          gl.glColor3f(c1,c2,c3);
          gl.glTranslatef(0, 1f, 0);
          glut.glutSolidCube(size);
          gl.glColor3f(c4,c5,c6);
          gl.glTranslatef(0, 1f, 0);
          glut.glutSolidCube(size);
          gl.glColor3f(0, 0,0);
          glut.glutWireCube(size);
        }
      
        gl.glPopMatrix();
        
    }
    public void DibujarColumnaSNB(int a,float x, float y , float z, float c4,float c5, float c6){
        gl.glPushMatrix();
        gl.glTranslatef(x, y, z);
        for (int i = 0; i < a; i++) {
          gl.glColor3f(c1,c2,c3);
          gl.glTranslatef(0, 1f, 0);
          glut.glutSolidCube(size);
          gl.glColor3f(c4,c5,c6);
          gl.glTranslatef(0, 1f, 0);
          glut.glutSolidCube(size);
        }
      
        gl.glPopMatrix();
        
    }
}
