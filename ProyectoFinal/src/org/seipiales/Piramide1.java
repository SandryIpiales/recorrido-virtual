/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.seipiales;

import javax.media.opengl.GL;

/**
 *
 * Cubo.java 
 * Autora:Sandra Ipiales.
 * Esta clase fue contiene la creacion de piramides en 
 * 3D con figuras gl
 * 2020-09-28
 */
public class Piramide1 {
 
    //coordenadas
    float x,y,z;
    //dimensiones
    float w,h,d; //ancho, alto, profundidad
    //colores
    float c1,c2,c3;
    //GL
    GL gl;
    //Variables de rotacion
    float angx, angy, angz;
    
   
    public Piramide1(GL gl,float x, float y, float z, float w, float h, float d, float c1, float c2, float c3, float angx, float angy, float angz) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
        this.h = h;
        this.d = d;
        this.c1 = c1;
        this.c2 = c2;
        this.c3 = c3;
        this.gl=gl;       
        this.angx= angx;
        this.angy= angy;
        this.angz= angz;
        
    }      
      
    public void dibujarPiramide(){
        gl.glPushMatrix();

        gl.glColor3f(c1,c2,c3);
        gl.glTranslatef(x, y, z);
        gl.glRotatef(angx, 1, 0, 0);
        gl.glRotatef(angy, 0, 1, 0);
        gl.glRotatef(angz, 0, 0, 1);

        gl.glScalef(w, h, d);
        gl.glBegin(GL.GL_QUADS);
        //BASE
        gl.glVertex3f(-1, -1, -1);
        gl.glVertex3f(1, -1, -1);
        gl.glVertex3f(1, -1, 1);
        gl.glVertex3f(-1, -1, 1);

        gl.glEnd();
        
       gl.glBegin(GL.GL_TRIANGLES);
        //Cara Lateral Derecha
        gl.glVertex3f(-1, -1, 1);
        gl.glVertex3f(0, 1, 0);
        gl.glVertex3f(-1, -1, -1);

        //Cara Lateral Izquierda
        gl.glVertex3f(1, -1, 1);
        gl.glVertex3f(0, 1, 0);
        gl.glVertex3f(1, -1, -1);

        //Cara  atras
        gl.glColor3f(c1 + 0.2f, c2 + 0.2f, c3 + 0.2f);
        gl.glVertex3f(1, -1, -1);
        gl.glVertex3f(0, 1, 0);
        gl.glVertex3f(-1, -1, -1);

        //Cara  delante
        gl.glVertex3f(1, -1, 1);
        gl.glVertex3f(0, 1, 0);
        gl.glVertex3f(-1, -1, 1);
        gl.glEnd();

        gl.glPopMatrix();
    
   
  }  
    
}
