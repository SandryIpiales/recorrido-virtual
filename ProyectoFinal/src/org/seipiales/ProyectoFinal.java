package org.seipiales;

import com.sun.opengl.util.Animator;
import com.sun.opengl.util.GLUT;
import java.awt.Frame;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCanvas;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.glu.GLU;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;



/**
 * ProyectoFinal.java 
 * Autora:Sandra Ipiales.
 * Este programa contiene la clase principal de este proyecto
 * 
 * 2020-09-28
 */
public class ProyectoFinal extends JFrame implements KeyListener{

    public static GL gl;
    public static GLU glu;
    public static GLUT glut;
   //Variables de translacion
    public static float radio,aradio,a;
    public  float tx, ty, tz , rx, ry, rz;
    //Variables de camara 
    public static int numcam=1;
    public  float camx, camy, camz, vistax, vistay, vistaz;
    //Objetos para implementar el entorno
    public static Caba�a casa, casap, chimenea;
    public static Piso p;
    public static Teclado teclado;
    public static Figuras f;
    public static Avatar avatar;
    public  static Arbol arbol;    
    public static Sphere p1;
    public static Calabaza calabaza, calabaza2;
    public static Interior interior;
    public static Cubo c;
    public static Puerta puerta;
    public static Mascota m;
    
    
    public ProyectoFinal(){
        setSize(1000,800);
        setLocation(10, 40);
        setTitle("Proyecto Casa de Hagrid");
//        setResizable(false);
        GraphicListener listener = new GraphicListener();
        GLCanvas canvas= new GLCanvas();
        gl= canvas.getGL();
        glu= new GLU();
        glut= new GLUT();
        canvas.addGLEventListener(listener);
        getContentPane().add(canvas);
        
        Animator animator= new Animator(canvas);
        animator.start();
        addKeyListener(this);
    }

    public static void main(String[] args) {
       ProyectoFinal frame = new ProyectoFinal();
       frame.setVisible(true);
       frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
   
    

    public class GraphicListener implements GLEventListener{
        
        //Inicializamos Variables
        public void init(GLAutoDrawable drawable) {
            GL gl = drawable.getGL();
            System.err.println("INIT GL IS: " + gl.getClass().getName());

            // Enable VSync
            gl.setSwapInterval(1);
        
            // Setup the drawing area and shading mode
            gl.glClearColor(0.096f, 0.2379f,0.32f,0);
            gl.glShadeModel(GL.GL_SMOOTH);
            //Instanciamos clase Figuras para agregar figuras geometricas 
               
             //////////*Istanciamos objetos *///////////
            casa= new Caba�a(gl,glu,glut,25,-3f,0.4f,-9);
            casap=  new Caba�a(gl,glu,glut,0, -3.5f,0.2f,-0f);
            chimenea= new Caba�a(gl,glu,glut,90,4.5f,-7,.5f);
            teclado = new Teclado(); 
            f= new Figuras ();
            p = new Piso(gl,glu,glut,0,0f,0);
            avatar = new Avatar(gl,glu,glut,-8,-3,-15);
            arbol= new Arbol(gl,glu, glut,2f,7.2f,-10);
            p1 = new Sphere(gl,glut, 2, 15, 5, 0,1,0);
            calabaza= new Calabaza(gl,glut,0.8f,10,9,0.73f,0.4575f,0.146f, -5,-2,10);
            calabaza2= new Calabaza(gl,glut,1.f,10,9,0.73f,0.4575f,0.146f, -5,-2,10);
            interior= new Interior(gl,glu, glut, 4,-0.5f,-4);
            puerta= new Puerta(gl, glut, 0.45f,0.3206f,0.13f);
            m= new Mascota(gl,glut, -7,-2,0);
        }
        
        public void display(GLAutoDrawable arg0) {
            GL gl = arg0.getGL();
            glu= new GLU();
            glut= new GLUT();
            //
            gl.glClear(GL.GL_COLOR_BUFFER_BIT |  GL.GL_DEPTH_BUFFER_BIT);
            //Buffer de Profundidad
            gl.glEnable(GL.GL_DEPTH_TEST);
//            gl.glEnable(GL.GL_LIGHTING);
            gl.glLoadIdentity();
            

            //*Control Teclas*// 
            
            tx= Teclado.getTrasladaX() ;
            ty = Teclado.getTrasladaY();
            tz= Teclado.getTrasladaZ();
            ry= Teclado.getRotarX();
            
            
            ///////////////*Manejo de Camaras* ///////////
            if (numcam==1) {
                //Camara automatica        
                avatar.movimiento_automatico();
                glu.gluLookAt(avatar.x - 3.3, avatar.y + 5, avatar.z+10, avatar.x + 17, avatar.y + 1, vistaz, 0, 1, 0);
                avatar.Hagrid();
            }
            if (numcam==2) {
                /*C�mara panor�mica*/
                camx= -30;
                camy= 14;
                camz= 9;
                glu.gluLookAt(camx, camy, camz, 0,0,0,0, 1,0);
                avatar.Hagrid();
            }
            /*C�mara que se mueva con teclas*/
            if (numcam==3) {

                avatar.x= -8+tx;
                avatar.y= ty ;
                avatar.z= tz-8;
                glu.gluLookAt(avatar.x -5, avatar.y+1, avatar.z+8, 0,0,ry, 0, 1, 0);
                
                avatar.Hagrid();
               
            }
            if (numcam==4) {
            /*C�mara que gira al rededor de un objeto*/
             
            radio = 25;
            a = a + 0.005f;

            camx = radio * (float)Math.cos(a);
            camz = radio * (float)Math.sin(a);

            glu.gluLookAt(casa.x + camx, 7, camz, casa.x, 0, casa.z + 4, 0, 1, 0);
            avatar.Hagrid();
            }
            if (numcam==5) {
            /*C�mara con trayectoria circular variable*/
             
            radio = tx;
            a = a + 0.005f;
            camx = radio * (float)Math.cos(a);
            camz = radio * (float)Math.sin(a);
            glu.gluLookAt(casa.x + camx-8, 7, camz, casa.x, 0, casa.z + 4, 0, 1, 0);
            avatar.Hagrid();

            }
            
            ///////////*Colocacion de Elementos del Escenario* ///////////
            
               gl.glScalef(0.5f,0.5f,0.5f);
               p.BasePiso();
               arbol.Bosque();
               casap.CasaPeque�a();
               calabaza2.Calbaza3(0, 0, 0);
               calabaza2.Calbaza3(1, 0, -8);
               chimenea.Chimenea(14,2);
               casa.CasaGrande();
               calabaza.CalabazasD();
               interior.InteriorCasa();
               puerta.DibujarPuerta(-4.5f, 0.5f, -6.5f);
               puerta.DibujarPuertap(-4.5f, 0.5f, -6.5f,35);
               m.Dibujar();

            gl.glFlush();
        }

        public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
        GL gl = drawable.getGL();
        GLU glu = new GLU();
        glut=new GLUT();

        if (height <= 0) { // avoid a divide by zero error!
        
            height = 1;
        }
        final float h = (float) width / (float) height;
        //Establecemos los parametros de proyeccion
        gl.glViewport(0, 0, width, height);
        gl.glMatrixMode(GL.GL_PROJECTION);
        gl.glLoadIdentity();
        glu.gluPerspective(30.0f, 1, 1.0, 50.0);
        gl.glMatrixMode(GL.GL_MODELVIEW);
        gl.glLoadIdentity();
        }

        public void displayChanged(GLAutoDrawable drawable, boolean modeChanged, boolean deviceChanged) {
        }

    }
        public void keyTyped(KeyEvent ke) {
            teclado.keyTyped(ke);
         }

        public void keyPressed(KeyEvent ke) {
            teclado.keyPressed(ke);
        }

        public void keyReleased(KeyEvent ke) {
        }
}
