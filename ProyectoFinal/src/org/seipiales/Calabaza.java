/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.seipiales;

import com.sun.opengl.util.GLUT;
import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;
import static org.seipiales.ProyectoFinal.calabaza2;

/**
 *
 * Calabaza.java 
 * Autora:Sandra Ipiales.
 * Este programa contiene los metodos para 
 * dibujar calabazas
 * 2020-09-28
 */
public class Calabaza {
    //Variables GL,GLUT
    GLU glu;
    GLUT glut;
    GL gl;
    //Variables de translacion
    float x, y, z;
    //Variables de color
    float c1,c2,c3;
    //Variables para dibujar Sphere
    double size;
    int slices, stacks;
    //Objetos
    Cilindro tronco, t;
    Cadena cadena;
    
    public Calabaza(GL gl,GLUT glut, double size, int slices, int stacks, float c1, float c2, float c3, float x,float y,float z){
        this.gl=gl;
        this.glut=glut;
        this.size=size;
        this.slices=slices;
        this.stacks= stacks;
        this.c1=c1;
        this.c2=c2;
        this.c3=c3;
        this.x= x;
        this.y=y;
        this.z=z;
        tronco= new Cilindro (gl,glut,0.4f, 6, 10, 10,0.35f,0.1948f,0.0175f);
        t= new Cilindro(gl,glut,0.15f, 0.8f, 10, 10,0,0,0);
        cadena= new Cadena(gl, glut);

    }
    
    public void DibujarSphereB(float x, float y , float z){
        gl.glPushMatrix();
        gl.glTranslatef(x, y, z);        
        gl.glRotatef(60, 1,1,0);
        gl.glColor3f(c1,c2,c3);
        glut.glutSolidSphere(size,slices, stacks);
         gl.glColor3f(0.44f,0.3065f,0.154f);
        glut.glutWireSphere(size,slices, stacks);
        gl.glPopMatrix();
    }
    
    public void DibujarCalabaza(float ang, float r){
         gl.glPushMatrix();
         //Calabazas posicion de circulo
        gl.glTranslatef(x, y, z); 
        gl.glRotatef(ang, 0,1,0);
        for (int j = 0; j < 6; j++) {
            DibujarSphereB(r*(float)Math.cos(j),0,r*(float)Math.sin(j) ) ;
        } 
        gl.glPopMatrix();
    }
    
    public void CalabazasD(){
        gl.glPushMatrix();
        DibujarCalabaza(0,4);
        DibujarCalabaza(85,4);
        DibujarCalabaza(0,2);
        gl.glPushMatrix();
        gl.glTranslatef(0, 1, 0);
        DibujarCalabaza(0,2);
        gl.glPopMatrix();
        gl.glPushMatrix();
        tronco.DibujarCilindro(90,-5f,2,9.4f);
        t.DibujarCilindro(90, -5f, 2.5f, 9.4f);
        gl.glPopMatrix();
        gl.glPushMatrix();
        cadena.DibujarCad(-4.7f,2.3f,9.7f,0,0,0);
        t.DibujarCilindro(90, -5f, 2.5f, 9.4f);
        gl.glPopMatrix();
        gl.glPopMatrix();
    }
    public void Calbaza3(float x, float y, float z){
        gl.glPushMatrix();
        //Calabazas 3 una encima de otra
            gl.glTranslatef(x, y, z);
            calabaza2.DibujarSphereB(-6, -1.2f, -2);
            calabaza2.DibujarSphereB(-6, -1.2f, -4);
            calabaza2.DibujarSphereB(-4, -0.5f, -3);
        gl.glPopMatrix();
    }
}
