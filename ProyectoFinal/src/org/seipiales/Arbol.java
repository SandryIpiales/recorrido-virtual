/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.seipiales;

import com.sun.opengl.util.GLUT;
import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;

/**
 * Arbol.java
 * Autor: Sandra Ipiales
 * Esta clase crea Arboles, for de Arboles para formar el bosque 
 * 2020-09-29
 * */
public class Arbol {
    GL gl;
    GLU glu;
    GLUT glut;
    float x,y,z;
    Piramide1 hojas, hojas2, hojas3;
    Cilindro tronco;
    Figuras muro;
    
    public Arbol(GL gl, GLU glu, GLUT glut,float x, float y, float z){
       this.gl=gl;
        this.glu=glu;
        this.glut=glut;
        this.x=x;
        this.y=y;
        this.z=z; 
        hojas= new Piramide1(gl,7,0,0,2f,2f,1,0.2364f,0.37f,0.0259f,0,0,0);
        hojas2= new Piramide1(gl,7,-1,0,2f,2f,1,0.2364f,0.37f,0.0259f,0,0,0);
        hojas3= new Piramide1(gl,7,-2,0,2f,2f,1,0.2364f,0.37f,0.0259f,0,0,0);
        tronco= new Cilindro (gl,glut,0.3f, 4, 10, 10,0.35f,0.1948f,0.0175f);
        muro= new Figuras();
    }
    
    public void Bosque(){
        gl.glPushMatrix();
        gl.glTranslatef(x, y, z); 
        gl.glScalef(1.5f, 1.5f,1.5f);
        //Bosque Horizontal
       ArbolHor(4,0,1);
       ArbolHor(9,0,0.5f);
       //Bosque Vertical
       ArbolVer(-19,-10,-5);
       ArbolVer(-19,-10,19);
       muro.dibujarCubo(gl, -1.8f,-6.5f,-5,25f ,2f, 2f, 0.3948f, 0.45f, 0.036f);
        gl.glPopMatrix();
    }
   
    public void ArbolHor(float x, float y, float z){
        gl.glPushMatrix();
        float posx=0;
        for (int i = 0; i < 6; i++) {
            Arbol(0,0,0+posx);
            Arbol(0+x,0,z+posx);
            posx+=3f;
        }
        muro.dibujarCubo(gl,4f, -5.7f, 8,0.5f, 2f, 23f, 0.54f,0.5346f,0.5346f);
        gl.glPopMatrix();
    }
     
    public void ArbolVer(float x, float y, float z){
        gl.glPushMatrix();
        gl.glTranslatef(x, y, z);
        float posx=0;
        for (int i = 0; i < 7; i++) {
             gl.glTranslatef(posx, 0, 0);
            Arbol(0,10,0);
            posx+=1;
        }
         gl.glRotatef(90, 0,1,0);
         muro.dibujarCubo(gl,-1.4f, 4.3f, -7.5f,0.5f, 2f, 19f, 0.54f,0.5346f,0.5346f);
         gl.glPopMatrix();
    }
    public void Arbol(float x, float y , float z){
        gl.glPushMatrix();
        gl.glTranslatef(x, y, z); 
        tronco.DibujarCilindro(90, 7, -3, 0);
        hojas.dibujarPiramide();
        hojas2.dibujarPiramide();
        hojas3.dibujarPiramide();
        gl.glPopMatrix();
    }
}
