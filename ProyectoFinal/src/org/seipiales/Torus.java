/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.seipiales;

import com.sun.opengl.util.GLUT;
import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;

/**
 *
 * Torua.java 
 * Autora:Sandra Ipiales.
 * Esta clase contiene la figura GLUT torus 
 * 2020-09-28
 */
public class Torus {
    GLU glu;
    GLUT glut;
    GL gl;
    double ri, rf;
    int sides, ring;
    
    public Torus(GL gl,GLUT glut, double ri, double rf,int sides, int ring ){
        this.gl=gl;
        this.glut=glut;
        this.ri=ri;
        this.rf= rf;
        this.sides= sides;
        this.ring=ring;
        
     
    }
    
    public void DibujarTorus(float x, float y , float z,float c1, float c2, float c3, float ang){
        gl.glPushMatrix();
        gl.glTranslatef(x, y, z);
        gl.glRotatef(ang, 0,1,1);
        gl.glColor3f(c1,c2,c3);
        glut.glutSolidTorus(ri, rf, sides, ring);
        gl.glPopMatrix();
    }
    public void DibujarTorus(float x, float y , float z,float c1, float c2, float c3, float angx, float angy, float angz){
        gl.glPushMatrix();
        gl.glTranslatef(x, y, z);
        gl.glRotatef(angx, 1,0,0);
        gl.glRotatef(angy, 0,1,0);
        gl.glRotatef(angz, 0,0,1);
        gl.glColor3f(c1,c2,c3);
        glut.glutSolidTorus(ri, rf, sides, ring);
        gl.glPopMatrix();
    }
}
