/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.seipiales;

import com.sun.opengl.util.GLUT;
import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;

/**
 *
 * Caba�a.java 
 * Autora:Sandra Ipiales.
 * Esta clase contiene las dos caba�as que forman parte del escenario
 * 
 * 2020-09-28
 * 
 */
public class Caba�a {
    GL gl;
    GLU glu;
    GLUT glut;
    Cilindro c;
    Cono techo, techog;
    Cubo columna1,columna2,columna3, graderio,graderio1,piso,sop, chim, c1;
    float x,y,z, ang;
    Figuras f, v1, f2;
    
    public Caba�a(GL gl, GLU glu, GLUT glut,float ang ,float x, float y, float z){
        this.gl=gl;
        this.glu=glu;
        this.glut=glut;
        this.ang = ang;
        this.x=x;
        this.y=y;
        this.z=z;
        c= new Cilindro(gl, glut, 3, 4, 12, 12, 0.6402f, 0.6587f, 0.66f);
        techo= new Cono(gl, glut, 14.1f, 19f, 24,24, 0.2904f, 0.33f, 0.264f);
        techog= new Cono(gl, glut, 21.5f, 13f, 24,24, 0.2904f, 0.33f, 0.264f);
        columna1 = new Cubo(gl,glut,2f,0.27f,0.2675f,0.2403f);
        columna2= new Cubo(gl,glut,2f,0.27f,0.2675f,0.2403f);
        columna3= new Cubo(gl,glut,2f,0.25f,0.2229f,0.1875f);
        graderio= new Cubo(gl,glut,3f,0f,0f,0f);
        graderio1= new Cubo(gl,glut,3f,0.2904f, 0.33f, 0.264f);
        piso = new Cubo(gl, glut, 10, 0.27f,0.2675f,0.2403f);
        sop= new Cubo(gl,glut,5f,0.2904f, 0.33f, 0.264f);
        f= new Figuras();
        v1= new Figuras();
        chim= new Cubo(gl,glut,1,0.38f,0.38f,0.38f);
        c1=new Cubo(gl,glut,2f,0.27f,1,0.2403f);
    }
  
    public void CasaGrande(){
          //Casa Grande
        gl.glPushMatrix();
        gl.glTranslatef(x, y, z);
        gl.glRotatef(ang, 0,1,0);
        gl.glScalef(0.1f,0.15f, 0.1f);
        
        gl.glRotatef(-80, 0,1,0);
        
        gl.glPushMatrix();
        gl.glRotatef(180, 0,1,-1);
        f.Cuadrado(gl, -38, 3f, 14f, 26f, 32f, 0.2904f, 0.33f, 0.264f);
        gl.glPopMatrix();
        
        gl.glPushMatrix();
        gl.glTranslatef(4, 13.5f, 0);
        /*Soporte de la casa*/
        SoporteCasa(5,3.9f,3.9f,3.9f,0,0,0);
        gl.glPopMatrix();
        gl.glPushMatrix();
        Graderio(13,21f, -6.5f, 2f);
        ParedesG(4,0,0);
        gl.glPopMatrix();        
        ///*Techo*///
        gl.glPushMatrix();
        gl.glScalef(2.1f,3.9f,2.5f);        
        techog.DibujarCono(-90, 18.5f, 4.1f, -5f);
        gl.glPopMatrix();
        
        gl.glPopMatrix();
    }
    public void Chimenea(int n, int p){
        gl.glPushMatrix();
        gl.glRotatef(ang, 0,1,0);
        gl.glScalef(0.5f, 0.5f, 0.5f);
        float posx=0;
        for (int i = 0; i < p; i++) {
            chim.DibujarColumna(n,x+posx, y, z, 0.27f,0.2675f,0.2403f);
            posx+=1f;
        }
        gl.glPopMatrix();
    }
    public void CasaPeque�a(){
        //Casa Peque�a
        gl.glPushMatrix();
        gl.glTranslatef(x,y,z);
        
        gl.glScalef(0.1f,0.15f, 0.1f);
        gl.glRotatef(ang, 0,1,0);
        //Base
        gl.glPushMatrix();
        gl.glRotatef(180, 0,1,-1);
        f.Cuadrado(gl, -16, 3f, 11f,20f, 19f,0.2904f, 0.33f, 0.264f);
        gl.glPopMatrix();
        
        gl.glPushMatrix();
        /*Soporte de la casa*/
        SoporteCasa(5,2,2,2,0,0,0);
        /* Graderio de la casa*/
        Graderio(6,17f, -3.5f, 2f);
        /*Paredes de la casa*/
        Paredes();  
       
        gl.glPopMatrix();
        
        /*Techo de la casa*/
        gl.glPushMatrix();
        gl.glScalef(2.f, 2.0f, 2.0f);
        techo.DibujarCono(-90,8.3f, 8.1f, -1.2f);
        
        gl.glPopMatrix();
        gl.glPushMatrix();
        
        f.Cuadrado(gl, 0,0,-12,1,2, 0,0,0);
        gl.glPopMatrix();
     

        gl.glPopMatrix();
    }
    public void Pared(int angl,int a,float p, float q, float r){
        gl.glPushMatrix();
        float posx=0,posy=0; 
        gl.glTranslatef(p,q,r);
        gl.glRotatef(angl, 0, 1, 0);
        for (int i = 0; i <a; i++) {
            columna1.DibujarColumna(7,0+posx, -6f,0, 0.27f,0.2675f,0.2403f);
            columna2.DibujarColumna(7,2f+posx, -6f,0, 0.27f,0.2675f,0.2403f);
            posx+=4f;
        }
        gl.glPopMatrix();
    }
  
 
    public void Paredes(){
        gl.glPushMatrix();
        gl.glScalef(2, 2, 2);        
         
        Pared(-25,3,10.1f,0,-9.85f);
        Pared(0,3,0,0,-10f);
        Pared(70,3,15.8f,0,4.5f);    
        columna3.DibujarColumna(7,-1, -6f, 1.8f, 0.25f,0.2229f,0.1875f);                  
        
        Pared(25,3,6.6f,0,9f);
        Pared(90,3,0,0,0); 
        columna3.DibujarColumna(7,6, -6.3f, 9.5f, 0.25f,0.2229f,0.1875f);    

        gl.glPopMatrix();
    }
    
    public void ParedesG(float p, float q, float r){
        gl.glPushMatrix();
        gl.glTranslatef(p, q, r);
        gl.glScalef(2, 2, 2);      
        Pared(75,5,32.8f,0,7f);       
        Pared(-25,5,19.5f,0,-18.8f);
        Pared(0,5,0,0,-19f);       
        Pared(90,5,0,0,0);
        columna3.DibujarColumna(7,0, -6f, 2, 0.25f,0.2229f,0.1875f);          
        Pared(28,6,13f,0,17.7f);
        columna3.DibujarColumna(7,11f, -6f, 18f, 0.25f,0.2229f,0.1875f);
        gl.glPopMatrix();
    }
    
    public void SoporteCasa(int p,float a, float b, float c, float x, float y, float z){
        gl.glPushMatrix();
        gl.glTranslated(x, y, z);
        gl.glScalef(a,b,c);
        SoporteC(90,p,0,0,0);
        SoporteC(0,p,0,0,-10f);
        SoporteC(-25,p,10.1f,0,-9.85f);
        SoporteC(70,p,15.2f,0,3f);
        SoporteC(30,p,6.6f,0,9f);
        gl.glPopMatrix();
    
    }
    
    public void Graderio(int a, float p, float q , float r){
        gl.glPushMatrix();
        float posx=0,posy=0;
        gl.glTranslatef(p,q,r);
        gl.glRotatef(-55, 0,1,0);       
        for (int i = 0; i < a; i++) {
            graderio.DibujarCubo(-7+posx, -9f, 16);
            graderio.DibujarCubo(-7+posx, -9f, 13);
            graderio1.DibujarCubo(-7+posx, -12.25f, 16);
            graderio1.DibujarCubo(-7+posx, -12.4f, 19);
            graderio1.DibujarCubo(-7+posx, -12.4f, 13);
            posx+=3;
        }           
        
        gl.glPopMatrix();
    }
    
    
  
    public void SoporteC(int angl, int a, float p, float q, float r){
       gl.glPushMatrix();
        float posx=0,posy=0; 
        gl.glTranslatef(p,q, r);
        gl.glRotatef(angl, 0, 1, 0);
        for (int i = 0; i<a; i++) {
            columna1.DibujarCubo(0+posx, -7.1f,0);
            posx+=2f;
        }
        gl.glPopMatrix();
    }
   
}
    
    
