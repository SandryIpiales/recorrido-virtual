/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.seipiales;

import com.sun.opengl.util.GLUT;
import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;

/**
 *
 * Avatar.java 
 * Autora:Sandra Ipiales.
 * Esta clase contiene el avatar del escenario principal
 * 
 * 2020-09-28
 */
public class Avatar {
    GL gl;
    GLU glu;
    GLUT glut;
    float x,y,z;
    Cubo chaqueta;
    Figuras cuerpo, cabeza, correa;
    Sphere cabello, cara;
    Figuras barba;
    float rx, ry, rz;
    
    public Avatar(GL gl, GLU glu, GLUT glut,float x, float y, float z){
        this.gl=gl;
        this.glu=glu;
        this.glut=glut;
        this.x=x;
        this.y=y;
        this.z=z;
        chaqueta= new Cubo(gl,glut,1.5f,0.47f,0.2519f,0.0188f);
        cabello= new Sphere(gl,glut,0.84f, 10, 10,0.45f,0.3206f,0.1125f);
        cara= new Sphere(gl,glut,0.8f, 10, 10,0.45f,0.3206f,0.1125f);
        barba= new Figuras();
        cuerpo= new Figuras();
        cabeza= new Figuras();
        correa= new Figuras();
    }
   
    public void Hagrid(){
        gl.glPushMatrix();
        gl.glScalef(0.5f,0.5f,0.5f);
        gl.glTranslatef(x, 2, z);
        gl.glRotatef(-90, 0,1,0);
       gl.glScalef(1.3f, 1.3f, 1.3f);
        gl.glPushMatrix();
        //*Creacion del Cabello *//
        gl.glScalef(1, 1, 0.5f);
        cabello.DibujarSphere(7,-0.55f,0);
        gl.glPopMatrix();
        cabeza.dibujarCubo(gl, 7,-0.8f,0, 1.5f,1f,0.8f,0.45f,0.3206f,0.1125f );
        cabeza.dibujarCubo(gl, 7,-0.8f,0.4f, 1f,0.7f,1.f,0.45f,0.3206f,0.1125f );
        //*Cuerpo*//
        cuerpo.Cuadrado(gl, 7,-1.1f,0, 0.76f, 0.45f, 0.45f,0.3206f,0.1125f);
        chaqueta.DibujarColumnaSNB(1, 7, -4, 0, 0.47f,0.2519f,0.0188f);
        cuerpo.Cuadrado(gl, 7,-2.4f,0.8f, 0.5f, 1.33f, 0.47f,0.19f,0.0188f);
        cabeza.Circulo(gl, 7, -0.6f, -0.5f, 0.94f,0.7152f,0.5546f,7,-0.6f,0.45f);
        //*Correa*//
        correa.Cuadrado(gl, 7,-2.4f,0.85f,0.5f, 0.2f,  0.32f,0.1613f, 0.048f);
        correa.Cuadrado(gl, 7,-2.4f,0.9f,0.2f, 0.2f,0.13f, 0.0648f, 0.0182f);
        cabeza.Cuadrado(gl, 7,-0.8f,1f,0.1f, 0.05f,0.94f,0.7152f,0.5546f);
        //*Barba*//
        gl.glScalef(0.5f, 1f, 0.1f);
        barba.Cuadrado(gl, 14f,-1.1f,8.5f,0.5f, 0.4f,0.45f,0.3206f,0.1125f);
        barba.Cuadrado(gl, 14.5f,-1f,8.5f,0.5f, 0.4f,0.45f,0.3206f,0.1125f);
        barba.Cuadrado(gl, 13.5f,-1f,8.5f,0.5f, 0.4f,0.45f,0.3206f,0.1125f);       
        gl.glPopMatrix();
       
        
    }
    public void movimiento_automatico() {
        this.z -= .01;
        System.out.println("Z"+z);
        if (this.z <-18) {
            this.x-= 0.03;
            System.out.println("x"+x);
        }
        if(this.z <-20 && x<-20 ){
            this.z-=0.01f;
          
            System.out.println("Z"+z);

        }
        if(this.z ==-27  ){
            this.z+=0.01f;
          
            System.out.println("Z"+z);

        }


    }

    public void movimiento_Panoramica() {
      
             this.x -= .03;
             System.out.println("Translacion en x: " + x);
             if(x<-12){
                 this.z-=0.03f;
                 System.out.println("Translacion en z: " +z);
             }
             if(z<-14){
                 this.x=0.03f;
                 System.out.println("Translacion en x: " + x);
             }if(x<-20){
                 this.z+=0.03f;
             }
             if(x<-26 && z<-13){
                 this.x=0.03f;
             }
             if(z>-23 && x>-16){
                 this.z-=0.03f;
             }
    }
}
