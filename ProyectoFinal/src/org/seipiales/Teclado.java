/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.seipiales;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.media.opengl.GL;

/**
 *
 * Teclado.java 
 * Autora:Sandra Ipiales.
 * Esta clase contiene los metodos de control de
 * teclado
 * 2020-09-28
 */
public class Teclado implements KeyListener {
     static GL gl;
    
      //Variables para la rotaci�n
    private static float rotarX = 0;
    private static float rotarY = 0;
    private static float rotarZ = 0;
    //Variables para la traslacion
    private static float trasladaX = 0;
    private static float trasladaY = 0;
    private static float trasladaZ = 0;
    
    public static float getTrasladaX(){
        return trasladaX;
    }
    public static float getTrasladaY(){
        return trasladaY;
    }
    public static float getTrasladaZ(){
        return trasladaZ;
    }
    public static float getRotarX(){
        return rotarY;
    }
    
    public void keyTyped(KeyEvent ke) {
        //Cambio de Camara
        if (ke.getKeyChar()== '1') {
            ProyectoFinal.numcam=1; 
        }
        if (ke.getKeyChar()== '2') {
            ProyectoFinal.numcam=2; 
        }
        if (ke.getKeyChar()== '3') {
            ProyectoFinal.numcam=3; 
        }
        if (ke.getKeyChar()== '4') {
            ProyectoFinal.numcam=4; 
        }
        if (ke.getKeyChar()== '5') {
            ProyectoFinal.numcam=5; 
        }
    }

    public void keyPressed(KeyEvent ke) {
        if(ke.getKeyCode()==KeyEvent.VK_ESCAPE){
            rotarX=0;
            rotarY=0;
            rotarZ=0;
            trasladaX=0;
            trasladaY=0;
            trasladaZ=0;
        }
     
        //Camara
        if(ke.getKeyCode()==KeyEvent.VK_RIGHT){
             trasladaX+=.10f;
            if(trasladaX>17){
              trasladaX-=.10f;
            System.out.println("Posicion de la camara en X: "+trasladaX);  
            }
            
        }

          if(ke.getKeyCode()==KeyEvent.VK_LEFT){
            trasladaX-=.10f;
            if(trasladaX<-16){
              trasladaX+=.10f;
            System.out.println("Posicion de la camara en X: "+trasladaX);  
            }
            System.out.println("Posicion de la camara en X: "+trasladaX);
        }

         if(ke.getKeyCode()==KeyEvent.VK_UP){
            trasladaY+=.10f;
            System.out.println("Posicion de la camara en Y: "+trasladaY);
        }

          if(ke.getKeyCode()==KeyEvent.VK_DOWN){
              trasladaY-=.10f; 
              if (trasladaY<-2.1) {
               trasladaY+=.10f;              
               System.out.println("Posicion de la camara en Y: "+trasladaY);

            
          }
              
          }
           if(ke.getKeyCode()==KeyEvent.VK_A){
               trasladaZ+=.10f;
               if(trasladaZ>16.4f){
                   trasladaZ-=.10f;
               }
            
            System.out.println("Posicion de la camara en Z: "+trasladaZ);
        }

          if(ke.getKeyCode()==KeyEvent.VK_S){ 
              trasladaZ-=.10f;
              if(trasladaZ<-15.4f){
                   trasladaZ+=.10f;
               }
           
            System.out.println("Posicion de la camara en Z: "+trasladaZ);
          }
           if(ke.getKeyCode()==KeyEvent.VK_X){
            rotarY+=.10f;
            System.out.println("Posicion de la camara en Y: "+trasladaY);
        }

          if(ke.getKeyCode()==KeyEvent.VK_C){
            rotarY-=.10f;
            System.out.println("Posicion de la camara en Y: "+trasladaY);
          }
         
         
   }

    public void keyReleased(KeyEvent ke) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
