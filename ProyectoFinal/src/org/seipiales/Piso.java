/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.seipiales;

import com.sun.opengl.util.GLUT;
import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;
import static org.seipiales.Figuras.x;
import static org.seipiales.Figuras.y;

/**
 *
 * Piso.java 
 * Autora:Sandra Ipiales.
 * Esta clase contiene la creacion de Piso
 * 2020-09-28
 */
public class Piso {
    GL gl;
    GLU glu;
    GLUT glut;
    Cubo piso ;
    Figuras c;
    Calabaza calabaza;
    float x,y,z;
    Cilindro p1;
    Cono pisoc;
   
    
    public Piso (GL gl,GLU glu, GLUT glut,float x,float y, float z){
        this.gl=gl;
        this.glu=glu;
        this.glut=glut;
        this.x=x;
        this.y=y;
        this.z=z;
        piso= new Cubo(gl,glut,15f,0.45f,0.3161f,0.027f);
        c= new Figuras();
        p1= new Cilindro(gl, glut, 1,3,5,5,0.47f,0.2724f,0.0611f);
        pisoc= new Cono(gl,glut ,10, 5, 10,10,0.47f,0.2724f,0);
    }    
    public void BasePiso(){
        gl.glPushMatrix();
        gl.glTranslatef(x, y, z);
        gl.glRotatef(180, 0, 1,1);
        //Base del Piso
        c.CuadradoPiso(gl, -0.3f, 0, -3.3f,25f, 25f, 0.3948f, 0.45f, 0.036f);  
        c.dibujarCubo(gl, -12.8f,3.2f,-2,10f ,32f, 3f, 0.3948f, 0.45f, 0.036f);
        c.CuadradoPiso(gl,9, -6.1f, -2.8f, 13f, 9f, 0.35f,0.1948f,0.0175f);
       c.dibujarCubo(gl, 0,-4.9f,-3, 10, 15, 1, 0.4f,0.2457f,0.044f);
       c.dibujarCubo(gl, 2,9.9f,-3.2f, 15, 12, 1, 0.4f,0.2457f,0.044f);
        gl.glPopMatrix();
       
    }
  
}
