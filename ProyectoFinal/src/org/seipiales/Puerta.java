/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.seipiales;

import com.sun.opengl.util.GLUT;
import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;

/**
 *
 * Puerta.java 
 * Autora:Sandra Ipiales.
 * Esta clase contiene el metodo para dinujar
 * puerta
 * 2020-09-28
 */
public class Puerta {
    GL gl;
    GLU glu;
    GLUT glut;
    //Color
    float c1, c2, c3;
    Figuras puerta;
    
    public Puerta(GL gl,GLUT glut, float c1, float c2, float c3){
        this.gl=gl;
        this.glut=glut;
        this.c1=c1;
        this.c2=c2;
        this.c3=c3;
        
        puerta= new Figuras();
    }
    
    public void DibujarPuerta(float x, float y , float z){
        gl.glPushMatrix();
        gl.glRotatef(-12f, 0,1,0);
        gl.glTranslatef(-1.1f, 0,1f);
        puerta.dibujarCubo(gl, x, y, z, 0.2f,4.7f,3.4f, c1, c2, c3);
        gl.glRotatef(90, 0,1,0);
        puerta.Cuadrado(gl, 5.1f,y, -4.7f,0.1f,2.2f , 0.35f,0.1948f,0.0175f);
        puerta.Cuadrado(gl, 8.1f,y, -4.7f,0.1f,2.2f ,0.35f,0.1948f,0.0175f);
        puerta.Cuadrado(gl,  6.5f,0.55f, -4.7f,0.1f,2.2f , 0.35f,0.1948f,0.0175f);        
        puerta.Cuadrado(gl,  5.8f,0.55f, -4.7f,0.1f,2.2f , 0.35f,0.1948f,0.0175f);                
        puerta.Cuadrado(gl,  7.2f,0.55f, -4.7f,0.1f,2.2f , 0.35f,0.1948f,0.0175f);
        puerta.Cuadrado(gl,  6.7f,2.7f, -4.7f,1.5f,0.1f ,0.35f,0.1948f,0.0175f);
        puerta.Cuadrado(gl,  6.7f,-1.6f, -4.7f,1.5f,0.1f , 0.35f,0.1948f,0.0175f);

        gl.glPopMatrix();
    }
    public void DibujarPuertap(float x, float y , float z, float ang){
        gl.glPushMatrix();
        gl.glTranslatef(4.3f,0.2f,2.4f);
        gl.glScalef(1, 0.9f,0.5f);
        gl.glRotatef(ang, 0,1,0);
        puerta.dibujarCubo(gl, x, y, z, 0.2f,4.4f,3, c1, c2, c3);
        gl.glRotatef(90, 0,1,0);
        puerta.Cuadrado(gl, 5.1f,y, -4.7f,0.1f,2.2f , 0.35f,0.1948f,0.0175f);
        puerta.Cuadrado(gl, 8.1f,y, -4.7f,0.1f,2.2f ,0.35f,0.1948f,0.0175f);
        puerta.Cuadrado(gl,  6.5f,0.55f, -4.7f,0.1f,2.2f , 0.35f,0.1948f,0.0175f);        
        puerta.Cuadrado(gl,  5.8f,0.55f, -4.7f,0.1f,2.2f , 0.35f,0.1948f,0.0175f);                
        puerta.Cuadrado(gl,  7.2f,0.55f, -4.7f,0.1f,2.2f , 0.35f,0.1948f,0.0175f);
        puerta.Cuadrado(gl,  6.7f,2.7f, -4.7f,1.5f,0.1f ,0.35f,0.1948f,0.0175f);
        puerta.Cuadrado(gl,  6.7f,-1.6f, -4.7f,1.5f,0.1f , 0.35f,0.1948f,0.0175f);

        gl.glPopMatrix();
    }
 
}
