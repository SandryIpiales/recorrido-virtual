/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.seipiales;

import com.sun.opengl.util.GLUT;
import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;

/**
 *
 * Mascota.java 
 * Autora:Sandra Ipiales.
 * Este programa contiene los metodos para construir
 * la mascota
 * 2020-09-28
 */
public class Mascota {
    //Variable gl,glu,glut
    GL gl;
    GLU glu;
    GLUT glut;
    //Variables de translacion
    float x,y,z;
    //Objetos de otras clases
    Sphere  cara,ala;
    Figuras alas,ojo;
    Piramide1 pico;
    Torus cuello;
    
    public Mascota(GL gl,GLUT glut, float x, float y , float z){
        this.gl=gl;
        this.glut=glut;
        this.x=x;
        this.y=y;
        this.z=z;
        
        //*Instanciamos los objetos*//
         cara= new Sphere(gl,glut,0.8f, 10, 6,0.38f,0.38f,0.38f);
         ala= new Sphere(gl,glut,1.19f, 8, 7,0.38f,0.38f,0.38f);
         pico= new Piramide1(gl,-8,2,11,0.3f,0.4f,0.4f,1,1,1,90,0,0);
         alas = new Figuras();
         cuello= new Torus (gl,glut,0.2f, 1f, 10, 8);
         ojo= new Figuras();
         
    }
    public void Dibujar(){
        gl.glPushMatrix();
        gl.glTranslatef(x, y, z);
        //Rostro de Mascota
        cara.DibujarSphereB(1,-8,2,10,0,0,0);
        //Pico
        pico.dibujarPiramide();
        //Metodo Ala
        ala(-7,0.3f,5.2f);
        gl.glScalef(0.4f, 3f, 0.4f);
        //Cuello
        cuello.DibujarTorus(-20.7f,0.4f,24.7f, 0.38f,0.38f,0.38f, 90,0,0);
       
        gl.glPopMatrix();
    }
    
    public void ala(float x, float y, float z){
        gl.glPushMatrix();
        gl.glTranslatef(x, y, z);
        float posx=0, posy=0; 
        gl.glRotatef(70, 0,1,0);
        gl.glPushMatrix();
       
        gl.glScalef(2,1,1);
        ala.DibujarSphereB(0.5f,-1.7f,0f,0,0,0,0);
        gl.glPopMatrix();
        gl.glRotatef(-25, 1,0,1);
        for (int i = 0; i < 10; i++) {
            alas.DibujarTriangulo(gl, 0,-0.3f,0.4f ,1+posx,0f,0.38f,0.389f,0.38f,-1.5f-posx,-0.2f-posy,0);
            posx+=0.3;
            posy+=0.1f;
        }
        
        gl.glPopMatrix();
    }
}
