/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.seipiales;

import com.sun.opengl.util.GLUT;
import javax.media.opengl.GL;
import javax.media.opengl.glu.GLU;
import javax.media.opengl.glu.GLUquadric;

/**
 *
 * Interior.java 
 * Autora:Sandra Ipiales.
 * Este programa contiene los componentes que se encuentran 
 * en el interior de la casa
 * 2020-09-28
 */
public class Interior {
    //Variable GL,GLUT
    GL gl;
    GLU glu;
    GLUT glut;
    //Variables de transformacion
    float x,y,z;
    //Variables de color
    float c1, c2,c3;
    //Objetos
    Cilindro soportem, m1;
    Cilindro sofa,sofab;
    Cilindro velas;
    Figuras sofac, r;
    Sphere  sofas;
    
    public Interior(GL gl, GLU glu, GLUT glut, float x, float y, float z){
        this.gl=gl;
        this.glu=glu;
        this.glut=glut;
        this.x=x;
        this.y=y;
        this.z=z;
        //*Inistanciamos Objetos*//
        soportem = new Cilindro(gl,glut,1.5f,0.4f, 20,20, 0,0,0);
        m1= new Cilindro(gl,glut, 0.3f, 1.8f, 20,20, 0.42f,0.2468f,0.0042f);
        sofac= new Figuras();
        sofas= new Sphere(gl,glut,0.5f, 7, 5, 0.46f,0.2392f,0.046f);
        sofa= new Cilindro(gl,glut,1.2f, 0.2f, 10,5, 0.46f,0.2392f,0.046f);
        velas= new Cilindro(gl,glut,0.1f, 0.7f, 10,5,1,1,1);
        r= new Figuras();

    }
    
    public void DibujarMesa(float p, float q, float r){
        //Construccion de Mesa
        gl.glPushMatrix();
        gl.glTranslatef(p,q,r);
        soportem.DibujarCilindro(90, 0,-0.7f,0);
        m1.DibujarCilindro(90, 0,-0.8f,0);
        gl.glPushMatrix();
        GLUquadric quad;
        quad =glu.gluNewQuadric();
        glu.gluQuadricDrawStyle(quad,GLU.GLU_FILL);
        gl.glTranslated(-0.34f,-3.3f,0);
        gl.glRotatef(90, 0,1,0);
        gl.glRotatef(180, 0,0,1);
        gl.glColor3f(0,0,0);
       glu.gluPartialDisk(quad, 0.5, 1, 50, 50, 90, 180);
     
       gl.glPopMatrix();
        gl.glPopMatrix();;
    }
    public void t(){
        gl.glPushMatrix();
        r.dibujarCubo(gl, -1,0,0, 1,2,1,0.46f,0.2392f,0.046f);
        gl.glPopMatrix();
    }
    public void Tetera(){
        gl.glPushMatrix();
        gl.glTranslatef(-8, 0.7f, -7);
        gl.glRotatef(45, 0,1,0);
        gl.glColor3f(0.38f, 0.3752f, 0.0912f);
        glut.glutSolidTeapot(0.5f);
        gl.glColor3f(0.34f, 0.3351f, 0.0912f);
        glut.glutWireTeapot(0.5f);
        
         gl.glPopMatrix();
    }
    public void InteriorCasa(){
        
        gl.glPushMatrix();
        gl.glTranslatef(x, y, z);
        gl.glScalef(0.5f, 0.5f,0.5f);
        //Tetera
        Tetera();
        //Mesa        
        DibujarMesa(-8,1,-7);
        //Sofa
        Sofa(-4,-2.5f,-2);
        gl.glPopMatrix();
    }
    
    public void Sofa(float p , float q, float r){
        gl.glPushMatrix();
        gl.glTranslatef(p, q, r);
         sofac.dibujarCubo(gl, -3.f, 1.9f, 0.1f, 1.7f,0.7f,1.5f, 0,0,0);
         //Construccion de Sofa
         gl.glPushMatrix();
         gl.glRotatef(90, 0, 1, 0);
         sofas.DibujarSphereB(2.5f,-1.2f, 2,-1.2f, 0,0,0f);
         sofas.DibujarSphereB(2.5f,1f, 2,-1.2f, 0,0,0);
         gl.glPopMatrix();
         sofa.DibujarCilindro(0,90,0,-2.2f,3,0);
         sofac.dibujarCubo(gl, -3f, 0.99f, 0.1f, 1.7f,1.2f,2.3f, 0.46f,0.2392f,0.046f);
        gl.glPopMatrix();
    }
    
  
}
